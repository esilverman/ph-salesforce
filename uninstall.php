<?php

// If uninstalled not called from WordPress, exit
if( !defined('WP_UNINSTALL_PLUGIN') )
  exit();

// Delete Settings
_log('uninstalling...');
delete_option('ph_salesforce_settings');

// Delete Posts
$args = array(
  'post_type' => array( 'ph_salesforce_projects' , 'ph_salesforce_staff' )
);
$posts = get_posts( $args );

$force_delete = true;
foreach ( $posts as $post ){
  _log($post, 'deleting post :'.$post->post_title );
  wp_delete_post( $post->ID, $force_delete );
}


/**
 * Used for PHP console logging
 *
 * If configured properly can be used with
 * Log Viewer WP plugin : http://wordpress.org/plugins/log-viewer/
 */
function _log( $data , $label = null , $backtrace = false ) {
  $output = '';

  if ( $label !== null ) {
    $output .= "\n\n--- [ $label ] ---\n";
  }
  
  switch (gettype($data)) {
    case "array":
    case "object":
      $output .= print_r($data, TRUE);
      break;
    case "integer":
    case "float":
    case "string":
      $output .= $data;
      break;
    case "boolean":
      $converted_data = ($data) ? 'true' : 'false';
      $output .= $converted_data;
      break;
    default:
      $output .= "ERROR: tried to log bad type [".gettype($data)."]";
      break;
       
  }
  
  if ( $backtrace ) {
    if ( !isset($label) )
      $label = '_log()';
    _backtrace('backtrace for '. $label );
  }
  
  if ( $label !== null ) {
    $output .= "\n---^^^^^^^ END $label ^^^^^^^---";
  }
  
  error_log($output);
  
}



?>