'use strict'
module.exports = (grunt) ->
  grunt.initConfig
    jshint:
      all: ['Gruntfile.js']

    coffee:
      glob_to_multiple: {
        expand: true
        flatten: true
        bare: true
        cwd: 'src/coffee'
        src: ['*.coffee']
        dest: 'assets/js'
        ext: '.js'
      },

    watch:
      coffee:
        files: ['src/coffee/*.coffee'],
        tasks: ['coffee:glob_to_multiple','uglify:js'],
        options: 
          bare: true
      less:
        files: ['src/less/*.less'],
        tasks: ['less:style'],
      css:
        files: ['*.css']
      livereload:
        files: ['assets/css/admin.css', 'assets/js/admin.js'],
        options:
          livereload: true

    uglify:
      js:
        files:
          'assets/js/admin-min.js': ['assets/js/admin.js']

    less:
      style:
        files:
          'assets/css/ph-salesforce.css': 'src/less/ph-salesforce.less'
          'assets/css/admin.css': 'src/less/admin.less'

    clean:
      build :
        'assets/js/'


  grunt.loadNpmTasks 'grunt-contrib-jshint'
  grunt.loadNpmTasks 'grunt-contrib-coffee'
  grunt.loadNpmTasks 'grunt-contrib-watch'
  grunt.loadNpmTasks 'grunt-contrib-uglify'
  grunt.loadNpmTasks 'grunt-contrib-less'
  grunt.loadNpmTasks 'grunt-contrib-clean'
  
  # By default, lint and run all tests.
  grunt.registerTask 'default', [
    'watch'
    'jshint'
    'uglify'
    'less'
  ]

  grunt.registerTask 'cleanjs', ['clean:build','watch']

  return