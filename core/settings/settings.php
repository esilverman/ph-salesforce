<?php
  $wrap_classes = array('ph-sf-admin-options','wrap');
?>
<div class="<?php echo implode(' ', $wrap_classes);  ?>">
    <?php screen_icon('tools') ;?>
    <h2>Project Homecoming Salesforce Details</h2>
    <?php settings_errors( self::SETTINGS_GROUP_NAME ); ?>
    <?php
      extract( $this->settings );
      $last_import_time = isset( $last_import_time ) ? $last_import_time : 0;
    ?>
    <?php if( !$last_import_time ) : ?>
    <h3>Last Import From Salesforce : The import script has not run yet.</h3>
    <?php else :
      // Use "Today" or "Yesterday" if applicable
      $day_name = '';
      $date = date('d/m/Y', $last_import_time);
      if( $date == date('d/m/Y') ) {
        $day_name = ' (Today)';
      } else if( $date == date( 'd/m/Y', time() - ( 24 * 60 * 60 ) ) ) {
        $day_name = ' (Yesterday)';
      }
    ?>
    <h3>Last Import From Salesforce on <?php echo date( 'm-j-Y \a\t g:i A', $last_import_time ) . $day_name; ?></h3>
    <p>If you'd like to review the results from the last import, you can view them below.</p>
    <a href="#" class="show-last-results-btn button-primary">Show Results</a>
    <div class="hidden last-results">
      <?php echo $this->echo_import_results(); ?>
    </div>
    <?php endif; ?>

    <div class="manual-import-section">
      <h3>Manual Import</h3>
      <p>If you're experiencing trouble with the import or if you just want to get the freshest data from Salesforce, click the button below.</p>
      <p>Please be patient as this might take some time...</p>
      <br />
      <a href="#" class="manual-refresh-btn button-primary">Refresh Data</a>

      <span class="spinner hidden"></span>

      <div class="response hidden">
        <ul class="response-list"></ul>
      </div>
    </div>

    <form class="ph_sf_options_form" name="options" method="POST" action="options.php">
      <input type="hidden" name="<?php echo self::SETTINGS_GROUP_NAME ;?>[has_imported]" value="<?php echo isset( $has_imported ) ? (int) $has_imported : 0;?>" />
      <input type="hidden" name="<?php echo self::SETTINGS_GROUP_NAME ;?>[last_import_time]" value="<?php echo $last_import_time;?>" />
        <?php settings_fields( self::SETTINGS_GROUP_NAME ); ?>
    </form>    
    
</div>
