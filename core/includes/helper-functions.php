<?php

// Print arrays to JS console immediately inline
// Different from console_log which collects logs (see below)
if( !function_exists( 'console_json' ) ) {

  function console_json($array, $title="json array") {
  	echo "<script type='text/javascript'>";
  	if ($title !== 0) {
  		echo "console.log('$title');";
  	}
  	echo	"console.log(".json_encode($array).");
  	</script>";
  }
}

// Alias for console_log
if( !function_exists( 'jslog' ) ) {

  function jslog($array, $title="logged var") {
    console_json($array, $title);
  }
}


/**
 * Different from console_json:
 * This function collects logs throughout the PHP process
 * And prints them as inline JS in the footer
 * if the LOG_TO_CONSOLE var == TRUE
*/
if( !function_exists( 'console_log' ) ) {

  function console_log($array, $title="logged var") {
    global $logs;
    
    if ( count($logs) == 0 ) {
      $logs = array(
        "<script type='text/javascript'>",
        "</script>"
      );
    }
    
    $backtrace = debug_backtrace();
    
    $fullArray = array(
        "\"$title\"" => $array
      , "From"     => $backtrace[0]['file']
    );
    
    array_splice($logs, count($logs) - 1, 0,
      "console.log(".json_encode($fullArray).");"
    );
    
  }
}


/**
 * This function sits in the footer and
 * prints the logs collected to the JS console
 */
if( !function_exists( 'print_logs' ) ) {

  function print_logs() {
    global $logs;
    
    if (LOG_TO_CONSOLE && $logs !== null )
      echo implode("\n\t",$logs) . "\n";
  }

}

/**
 * Used for PHP console logging
 *
 * If configured properly can be used with
 * Log Viewer WP plugin : http://wordpress.org/plugins/log-viewer/
 */
if( !function_exists( '_log' ) ) {
  function _log( $data , $label = null , $backtrace = false ) {

    // Prevent logging if we're not in debug mode
    if( !defined('PH_SF_DEBUG') || !PH_SF_DEBUG )
      return;

    // Stop execution and log to ChromePhp if the class exists
    if( class_exists('ChromePhp') ) {

      if( $label ) {
        $r = array(
          'label' => $label,
          'data' => $data
        );

        return ChromePhp::log( $r );
      }

      return ChromePhp::log( $data );
    }


    $output = '';
    
    switch (gettype($data)) {
      case "array":
      case "object":
        $output .= print_r($data, TRUE);
        
        if ( $label !== null )
          $output .= "\n\n---^^^^^^^ END $label ^^^^^^^---";    
        
        break;
      case "integer":
      case "float":
      case "string":
        $output .= $data;
        break;
      case "boolean":
        $converted_data = ($data) ? 'true' : 'false';
        $output .= $converted_data;
        break;
      default:
        $output .= "ERROR: tried to log bad type [".gettype($data)."]";
        break;
         
    }
    
    if ( $backtrace )      
      _backtrace( $label );
    
    if ( !empty( $label ) ) {
      $output = "\t--- [ $label ] ---\n\n" . $output . "\n";
    }
    
    error_log($output);
    
  } // end _log()

} // end if exists _log()


if( !function_exists( '_log_v' ) ) {

  // "Verbose-only" alias of _log
  function _log_v( $data , $label = null , $backtrace = false ) {

    global $color_formats;
    $verbose = getopt( "", array('verbose') ) || ( defined('PH_VERBOSE') && PH_VERBOSE ) ;

    if( $verbose ) {
       if ( !in_array( gettype( $data ) , array( 'object', 'stdObject', 'array' ) ) ) {
         $data = sprintf( $color_formats['green'], $data );
       } else {
         $label = sprintf( $color_formats['green'], $label );
       }
      _log( $data, $label, $backtrace );
    }

  } // end _log_v()

} // end if exists _log_v()


if( !function_exists( '_backtrace' ) ) {
  function _backtrace( $label = '' ) {
    ob_start();
    if ( phpversion() >= 5.4 ) {
      debug_print_backtrace( 0 , 5); 
    } else {
      debug_print_backtrace();   
    }
    $trace = ob_get_contents(); 
    ob_end_clean();   
      
    _log( $trace, 'backtrace '. $label );
    _log("\t---^^^^^^^ END backtrace ^^^^^^^---");
  }
}

if( !function_exists( '_backtrace_files' ) ) {
  function _backtrace_files() {
    $backtrace = debug_backtrace();
    foreach( $backtrace as $key => $trace ) {
      if( isset( $trace['file'] ) )
        _log($trace['file']);
    }
  }
}

/**
 * Shorthand for custom post type query
 * 
 * Accepts the following params:
 * Custom Post Type - (string) $cptName
 * Limit for number of posts - (int) $posts_per_page
 * [Optional] Array of args used in WP_Query
 */
if( !function_exists( 'cpt_query' ) ) {
  function cpt_query($cptName, $posts_per_page=-1, $args=NULL) {
    $defaults = array(
      'post_type' => $cptName,
      'posts_per_page' => $posts_per_page
    );
    $args = wp_parse_args($args, $defaults);
    
    $cptQuery = new WP_Query($args);

    return $cptQuery;
  }
}

/*$
 * Return or echo post permalink by title
 */
if( !function_exists( 'permalink_by_title' ) ) {

  function permalink_by_title($title, $echo=true) {
  	
  	$id = get_page_by_title( $title );
  	
  	$link = get_permalink( $id );
  	
  	if (!$echo)
  	  return $link;
  	  
  	echo $link;
  }

}


function isset_or_false( $val ) {

  return isset( $val ) ? $val : false;

} // END isset_or_false() 


function not_empty_or_false( $val ) {

  return !empty( $val ) ? $val : false;

} // END not_empty_or_false() 


/*==========  Memory Usage / Tracking  ==========*/


if( !function_exists( '_mem' ) ) {

  function _mem( $input, $color_format = 0 ){

    global $mem_usage;
    $new_usage = memory_get_peak_usage();
    $diff = $new_usage - $mem_usage ;
    $mem_usage = memory_get_peak_usage();
  /*   if( $diff > 0 ) */
    $log = "+".( number_format( ($diff/1024) / 1024 , 2) ). " M [ ".( number_format( ($mem_usage/1024) / 1024 , 2) )." M total ] $input ( limit : ".ini_get('memory_limit'). " ) ";
    if( $color_format ) {
      $log = sprintf( $color_format , $log );
    }
    _log( $log );
  /*
    if( $diff > .005 )
      _log( "+".( $diff/1024/1024 ). " mb [ ".($mem_usage/1024/1024)." total mb ] $input " );
  */
  /*   if( $diff > 10000 )    _backtrace(); */
  }
}

if( !function_exists( '_mem_v' ) ) {

  // Alias of _mem that only echo's with --verbose flag on
  function _mem_v( $input ){

    global $color_formats;
    $verbose = getopt( "", array('verbose') ) || ( defined('PH_VERBOSE') && PH_VERBOSE ) ;

    if( $verbose ) {
      _mem( $input, $color_formats['cyan'] );
    }

  } // end _mem_v

} // end fn exists _mem_v

if( !function_exists( '_memstart' ) ) {

  function _memstart() {

    if( !ENABLE_MEM )
      return;
    
    _log( 'START MEMORY PROFILE' );
    $trace = debug_backtrace();
    _mem( $trace[1]['function'] );
  }

  function _memend() {

    if( !ENABLE_MEM )
      return;
    
    $trace = debug_backtrace();
    _mem( $trace[1]['function'] );
    _log( 'END MEMORY PROFILE' );

  }
}

/*==========  END : Memory Usage / Tracking  ==========*/

global $color_formats;
$color_formats = array(
  // styles
  // italic and blink may not work depending of your terminal
  'bold' => "\033[1m%s\033[0m",
  'dark' => "\033[2m%s\033[0m",
  'italic' => "\033[3m%s\033[0m",
  'underline' => "\033[4m%s\033[0m",
  'blink' => "\033[5m%s\033[0m",
  'reverse' => "\033[7m%s\033[0m",
  'concealed' => "\033[8m%s\033[0m",
  // foreground colors
  'black' => "\033[30m%s\033[0m",
  'red' => "\033[31m%s\033[0m",
  'green' => "\033[32m%s\033[0m",
  'yellow' => "\033[33m%s\033[0m",
  'blue' => "\033[34m%s\033[0m",
  'magenta' => "\033[35m%s\033[0m",
  'cyan' => "\033[36m%s\033[0m",
  'white' => "\033[37m%s\033[0m",
  // background colors
  'bg_black' => "\033[40m%s\033[0m",
  'bg_red' => "\033[41m%s\033[0m",
  'bg_green' => "\033[42m%s\033[0m",
  'bg_yellow' => "\033[43m%s\033[0m",
  'bg_blue' => "\033[44m%s\033[0m",
  'bg_magenta' => "\033[45m%s\033[0m",
  'bg_cyan' => "\033[46m%s\033[0m",
  'bg_white' => "\033[47m%s\033[0m",
);

?>