<?php
//Count sforce queries
session_start();
$_SESSION['queries']=0;
$_SESSION['proj_vol']=0;

date_default_timezone_set("America/Chicago");

// Shared hosting on HostGator doesn't allow this
ini_set('memory_limit','300M'); // HostGator shared maxes out at 256M
ini_set('max_execution_time','200'); // HostGator shared maxes out at 30 (seconds)

error_log('projects...');
$projects = get_projects();

$staff = get_unsuppressed_employees();
$fp = fopen('staff.json', 'w');
$staff['timestamp'] = date("Y-m-d g:i:s a");
fwrite($fp, json_encode($staff));
fclose($fp);

global $volunteers;
$volunteers = get_all_volunteers();
/* error_log(print_r($volunteers, true)); die(); */

error_log("Queries: " . $_SESSION['queries']);
_mem('donors...');
foreach($projects as $project_key => $project){
	$donors = get_project_donors( $project['Project_SF_ID__c'] );

	if( count( $donors ) ){
		$projects[ $project_key ]['Donors'] = $donors;
	}

}

error_log("Queries: " . $_SESSION['queries']);
_mem('Funding Partners...');
foreach( $projects as $project_key => $project ){
	$partners = get_funding_partners( $project['Project_SF_ID__c'] );
 	if( count( $partners ) ){
	  foreach ( $partners as $partner) {
	    if ( isset( $partner['Contact__c'] ) ) {
  	    $partner['Donor_Name'] = get_contact($partner['Contact__c']);
    		$projects[ $project_key ]['Funding_Partners'][] = $partner;
	    } else {
/*   	    _log($partner, 'partner does not have contact__c'); */
  		}
		} // END foreach partners
	} // END if count(partners)
} // END foreach projects

error_log("Queries: " . $_SESSION['queries']);
_mem('workrecords...');
foreach($projects as $project_key => $project){
	$workrecords[$project_key] = get_project_work_records($project['Project_SF_ID__c']);
	if(count($workrecords[$project_key]['Site_Managers'])){
		foreach($workrecords[$project_key]['Site_Managers'] as $sitemanager_key => $sitemanager){
			$projects[$project_key]['workrecords']['Site_Managers'][$sitemanager] = get_contact($sitemanager);
		}
	}
  $projects[$project_key]['workrecords']['Site_Managers'][$project['proj_cm__c']] = get_contact($project['proj_cm__c']);
}

error_log("Queries: " . $_SESSION['queries']);
_mem('building account lookup hash...');
$accounts = getAllAccounts();
$accountLookup = array();
foreach ($accounts as $account) {
	$accountLookup[$account['Id']]['name']= $account['Name'];
	$accountLookup[$account['Id']]['startDate']= $account['Start_Date__c'];
	$accountLookup[$account['Id']]['location'] = $account['Team_Reg_Location__c'];
}


error_log("Queries: " . $_SESSION['queries']);
_mem('volunteers...');
foreach($projects as $project_key => $project){
	if(count($workrecords[$project_key]['Volunteer_Registrations'])){
		foreach($workrecords[$project_key]['Volunteer_Registrations'] as $volreg_key){
      $teamreg = get_project_volunteer_single($volreg_key);
      $team_sf_ID = $teamreg[0]['Team_Reg__c'];

      if ( $team_sf_ID ){
  			$projects[$project_key]['workrecords']['Volunteer_Registrations'][$team_sf_ID] = array(
  				'contact' => $accountLookup[$team_sf_ID]['name'],
  				'startDate' => $accountLookup[$team_sf_ID]['startDate'],
  				'location' => $accountLookup[$team_sf_ID]['location']
  			);
			}
		}
	}
}

error_log("Queries: " . $_SESSION['queries']);
_mem("Project updates...");
foreach($projects as $project_key => $project){
  $updates = get_project_updates($project['Project_SF_ID__c']);  
	if(count($updates)){
		foreach($updates as $update){		  
			$projects[$project_key]['Updates'][ $update['Date__c'] ] = $update;
		}
	}
}
error_log("...end project updates");
_mem( __FILE__ . ' L ' . __LINE__ );

$fp = fopen('projects.json', 'w');
$projects['timestamp'] = date("Y-m-d g:i:s a");
fwrite($fp, json_encode($projects) );
fclose($fp);

$fp = fopen('volunteers.json', 'w');
$volunteers['timestamp'] = date("Y-m-d g:i:s a");
fwrite($fp, json_encode($volunteers));
fclose($fp);

// Remove all old cache folders
$hour_stamp = date("Y-m-d_H");
$cache_dir = "sf_cache_$hour_stamp";
$cache_path = "cache";
$iterator = new DirectoryIterator( dirname(__FILE__) . '/cache/' );
error_log("cache dir : $cache_path/$cache_dir/");
foreach ($iterator as $fileinfo) {
    $target_dir =  dirname(__FILE__) . "/cache/" . $fileinfo->getFilename() . "/";
    if (!$fileinfo->isDot()) {
      if( $fileinfo->getFilename() !== $cache_dir ) {
        error_log("deleting : $target_dir");

        if( $fileinfo->isDir() ) delete_files( $target_dir );

      } else {
        error_log("Saving : $target_dir" );      
      }
    }
}

/* 
 * php delete function that deals with directories recursively
 */
function delete_files($target) {
    if(is_dir($target)){
        $files = glob( $target . '*', GLOB_MARK ); //GLOB_MARK adds a slash to directories returned
        foreach( $files as $file ) {
            delete_files( $file );      
        }
        rmdir( $target );
    } elseif(is_file($target)) {
        unlink( $target );  
    }
} // END delete_files()

?>