<?php 

/**
*
* Publicly accessible functions to go along with the PH Sforce Plugin
*
**/

/**
 * Return all or some PH SF Projects.
 */
function get_ph_projects( $posts_per_page = -1, $args = NULL ) {
  global $ph_salesforce;

  $projects = cpt_query( PH_Project::CPT_NAME, $posts_per_page, $args ); 

  return count( $projects->posts ) ? $projects->posts : false;

} // END get_ph_projects()

function ph_project( $wp_id = NULL ) {

  if ( !$wp_id ) {
    global $post;
    $wp_id = $post->ID;
  }

  $project_args = array(
    'wp_id' => $wp_id
  );

  return new PH_Project( $project_args );

} // END ph_project() 




/**
 * Return all or some PH SF Employees.
 */
function get_ph_employees( $posts_per_page = -1, $args = NULL ) {
  global $ph_salesforce;

  $employees = cpt_query( PH_Employee::CPT_NAME, $posts_per_page, $args ); 

  return count( $employees->posts ) ? $employees->posts : false;

} // END get_ph_employees()


function ph_employee( $wp_id = NULL ) {

  if( !$wp_id ) {
    global $post;
    $wp_id = $post->ID;
  }

  // $details = array();

  $employee_args = array(
    'wp_id' => $wp_id
  );

  return new PH_Employee( $employee_args );

} // END ph_employee() 


function stored_emply_details( $wp_id = NULL ) {
  global $ph_salesforce;

  if( !$wp_id ) {
    global $post;
    $wp_id = $post->ID;
  }

  return $ph_salesforce->stored_emply_details( $wp_id );

} // END stored_emply_details() 


function ph_volunteer_org( $wp_id = NULL ) {

  if( !$wp_id ) {
    global $post;
    $wp_id = $post->ID;
  }

  // $details = array();

  $vol_org_args = array(
    'wp_id' => $wp_id
  );

  return new PH_Volunteer_Org( $vol_org_args );

} // END ph_volunteer_org() 


// ACF Helper functions
function print_field( $field_name=0, $args = null ) {

  if( !$field_name ) return false;

  $defaults = array(
    'wrap_el' => 'div',
    'classes' => array('acf-field', str_replace( '_', '-', $field_name ) ),
    'before' => '',
    'after' => ''
  );
      
  $args = wp_parse_args( $args, $defaults );
  
  extract( $args, EXTR_SKIP );

  if ( get_field( $field_name ) ) :
    echo "<{$wrap_el} class='" . implode(' ', $classes ) . "'>" .
            $before . get_field( $field_name ) . $after .
          "</{$wrap_el}>";
  endif;

} // end print_field()


?>