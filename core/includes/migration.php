<?php

/*=====================================================
=            Migrate from Old Theme to New            =
=====================================================*/

global $ph_salesforce;

class PH_Migrator {

  const OLD_PROJECT_CPT_NAME      = 'projects';
  const OLD_PROJECT_ID_KEY        = '_sforce_project_id';

  public $old_project_id_hash     = array();

  const OLD_EMPLOYEE_CPT_NAME     = 'sf_staff';
  const OLD_EMPLOYEE_ID_KEY       = '_sforce_staff_id';
  public $old_employeee_id_hash   = array();

  public $old_projects;
  public $old_employees;

  function __construct() {

    $this->get_old_projects();

    $this->get_old_employees();
    
    $this->migrate();

  } // END __construct() 


  function get_old_projects() {
    
    $old_projects_query = cpt_query( self::OLD_PROJECT_CPT_NAME );
    $this->old_projects = $old_projects_query->posts;

    $this->build_old_project_id_hash();

  } // END get_old_projects() 


  function build_old_project_id_hash() {
    
    foreach ( $this->old_projects as $project ) :

      // Get the SF ID from the DB
      $sf_id = $this->get_sf_id( $project->ID, self::OLD_PROJECT_ID_KEY );

      // Save that data in a hash to reduce DB queries
      if( $project->ID && $sf_id ) {
        $this->old_project_id_hash[ $project->ID ] = $sf_id;
        $this->old_project_id_hash[ $sf_id ] = $project->ID;
      }

    endforeach;

  } // end build_old_project_id_hash()


  function get_old_project_cats( $sf_id = 0 ) {

    if( !$sf_id ) return false;

    // Don't try to get the categories if there's no corresponding post
    if( !isset( $this->old_project_id_hash[ $sf_id ] ) ) return false;

    $old_project_wp_id = $this->old_project_id_hash[ $sf_id ];

    $cats = wp_get_post_categories( $old_project_wp_id );

    return $cats;

  } // END get_old_project_cats() 

  function get_old_employees() {

    $old_employees_query = cpt_query( self::OLD_EMPLOYEE_CPT_NAME );
    $this->old_employees = $old_employees_query->posts;

    $this->build_old_employee_id_hash();

  } // END get_old_employees() 

  function get_new_employees() {
    
    $new_employees_query = cpt_query( PH_Employee::CPT_NAME );
    $this->new_employees = $new_employees_query->posts;

    return $this->new_employees;
  } // end get_new_projects()


  function build_old_employee_id_hash() {

    foreach ( $this->old_employees as $employee ) :

      // Get the SF ID from the DB
      $sf_id = $this->get_sf_id( $employee->ID, self::OLD_EMPLOYEE_ID_KEY );

      // Save that data in a hash to reduce DB queries
      if( $employee->ID && $sf_id ) {
        $this->old_employee_id_hash[ $employee->ID ] = $sf_id;
        $this->old_employee_id_hash[ $sf_id ] = $employee->ID;
      }

    endforeach;

  } // end build_old_employee_id_hash()


  function migrate() {

    _log('------ Linking posts to new Projects...');
    $this->link_posts_to_projects();

    _log('------ Linking posts to new Employees...');
    $this->migrate_employee_categories();
    $this->link_posts_to_employees();

    _log('------ Linking featured images to Projects...');
    $this->attach_featured_images_to_projects();

    _log('------ Linking featured images to Employees...');
    $this->attach_featured_images_to_employees();


  } // END migrate() 

  /**
   *  Link posts to projects via categorization
   */
  function link_posts_to_projects() {

    $new_projects = $this->get_new_projects();

    // Loop over each new project and assign the categories
    // from the old corresponding project
    foreach ( $this->new_projects as $new_project ) :

      $sf_id = $this->get_sf_id( $new_project->ID, PH_Project::ID_KEY );

      $old_project_cats = $this->get_old_project_cats( $sf_id );

      if( $old_project_cats ) {
        $append = true; // Setting append to 'true' will prevent overwrite of existing cats
        wp_set_post_categories( $new_project->ID, $old_project_cats, $append );
      }

    endforeach;

    _log('Done linking posts to projects...');

  } // END link_posts_to_projects() 


  function get_new_projects() {
    
    $new_projects_query = cpt_query( PH_Project::CPT_NAME );
    $this->new_projects = $new_projects_query->posts;

    return $this->new_projects;
  } // end get_new_projects()


  function migrate_employee_categories() {
    
    $new_employees = $this->get_new_employees();

    // Loop over each new employee and assign the categories
    // from the old corresponding employee
    foreach ( $this->new_employees as $new_employee ) :

      $sf_id = $this->get_sf_id( $new_employee->ID, PH_Employee::ID_KEY );

      $old_employee_cats = $this->get_old_employee_cats( $sf_id );

      if( $old_employee_cats ) {
        $append = true; // Setting append to 'true' will prevent overwrite of existing cats
        wp_set_post_categories( $new_employee->ID, $old_employee_cats, $append );
      }

    endforeach;

    _log('Done linking posts to projects...');   


  } // END migrate_employee_categories() 


  function get_old_employee_cats( $sf_id = 0 ) {

    if( !$sf_id ) return false;

    // Don't try to get the categories if there's no corresponding post
    if( !isset( $this->old_employee_id_hash[ $sf_id ] ) ) return false;

    $old_employee_wp_id = $this->old_employee_id_hash[ $sf_id ];

    $cats = wp_get_post_categories( $old_employee_wp_id );

    return $cats;

  } // END get_old_employee_cats() 


  function link_posts_to_employees() {

    $new_projects = $this->get_new_projects();

    // Loop over each new project and assign the categories
    // from the old corresponding project
    foreach ( $this->new_projects as $new_project ) :

      $sf_id = $this->get_sf_id( $new_project->ID, PH_Project::ID_KEY );

      $old_project_cats = $this->get_old_project_cats( $sf_id );

      if( $old_project_cats ) {
        $append = true; // Setting append to 'true' will prevent overwrite of existing cats
        wp_set_post_categories( $new_project->ID, $old_project_cats, $append );
      }

    endforeach;

    _log('Done linking posts to projects...');   


  } // END link_posts_to_employees() 


  function attach_featured_images_to_projects() {

    foreach ( $this->old_projects as $old_project ) :

      // Get the old thumbnail ID
      $old_project_img_ID = get_post_thumbnail_id( $old_project->ID );

      // Don't even bother if there's not thumb for the old one
      if ( !$old_project_img_ID ) continue;

      // Set it to the new project with corresponding SF ID
      $sf_id = $this->get_sf_id( $old_project->ID, self::OLD_PROJECT_ID_KEY );
      $new_project = PH_Project::get_project_from_sf_id( $sf_id );

      if( $new_project && $old_project_img_ID ) {
        _log("Linked img ID {$old_project_img_ID} to {$new_project->post_title} ");
        set_post_thumbnail( $new_project->ID, $old_project_img_ID );
        $post = array(
          'ID' => $new_project->ID,
          'post_status' => 'publish'
        );
        wp_update_post( $post );
      }

    endforeach;

  } // END attach_featured_images_to_projects() 



  function attach_featured_images_to_employees() {


    foreach ( $this->old_employees as $old_employee ) :

      // Get the old thumbnail ID
      $old_employee_img_ID = get_post_thumbnail_id( $old_employee->ID );

      // Don't even bother if there's not thumb for the old one
      if ( !$old_employee_img_ID ) continue;

      // Set it to the new project with corresponding SF ID
      $sf_id = $this->get_sf_id( $old_employee->ID, self::OLD_EMPLOYEE_ID_KEY );
      $new_employee = PH_Employee::get_employee_from_sf_id( $sf_id );

      if( $new_employee && $old_employee_img_ID ) {
        _log("Linked img ID {$old_employee_img_ID} to {$new_employee->post_title} ");
        set_post_thumbnail( $new_employee->ID, $old_employee_img_ID );
        $post = array(
          'ID' => $new_employee->ID,
          'post_status' => 'publish'
        );
        wp_update_post( $post );        
      }

    endforeach;


  } // END attach_featured_images_to_employees() 


  function get_sf_id( $post_ID = 0, $id_key = 0 ) {
    if( !$id_key || !$post_ID ) return false;

    // TODO : check against hash first
    // CODE GOES HERE

    $sf_id_meta = get_post_meta( $post_ID, $id_key );

    // _log( $sf_id_meta, "sf id meta for {$post_ID} / {$id_key}");

    return isset( $sf_id_meta[0] ) ? $sf_id_meta[0] : false;
      
  } // END get_sf_id() 


} // end class PH_Migrator



?>