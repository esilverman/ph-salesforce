<?php

if ( !class_exists('PH_Salesforce_API') ) {
class PH_Salesforce_API {

  function __construct() {

    global $ph_salesforce;
    
    session_start();
    $_SESSION['queries']=0;
    $_SESSION['proj_vol']=0;
    
    // Require config and initialize connection
    $config = require dirname( __FILE__ ) . '/config.php';
    $soapclient_path = $ph_salesforce->sf_api_path . '/soapclient/SforcePartnerClient.php';
    require_once $soapclient_path;
    $ph_sforce_connection = $this->sforce_login($config['username'],$config['password'] . $config['token']);

  }

  function get_projects(){

    $fields = array(
      'Name',
      'Completion__c',
      'Completion_Date__c',
      'proj_cm__c',
      'Neighborhood__c',
      'constructionstatus__c',
      'Current_Phase__c',
      'Estimated_Market_Cost__c',
      'Estimated_Savings__c',
      'Marketing_Story__c',
      'Program__c',
      'Start_Date__c',
      'Total_Non_Client_Funds__c',
      'Final_Project_Cost__c',
      'Project_SF_ID__c',
      'Volunteer_Hours__c',
      'Project_Address__c',
      'Suppress_from_Website__c',
      'Total_Donation_Goal__c',
      'Scope_of_Work_website__c',
      'Construction_Type__c',
      'Recipient_Type__c',
      'Total_Donations_Needed__c',
      'Total_Donations_Received__c',
      'Flickr_Link__c',
      'Homeowner_Story_Blog_Post__c'
    );

    $query = 'SELECT ' . implode($fields, ',') . ' from Project_Information__c WHERE Suppress_from_Website__c = false ';
    $results = $this->sf_query($query);
    return $results;
  }

  function get_project_donors( $id = 0 ){

    global $ph_salesforce;

    if( !$id ){
      $id = $ph_salesforce->current_project_sf_id;
    }

    $recordTypeID = '012F0000000mtgD';
    $fields = array('Amount','Check_Date__c','Dedication_Honoree_Name__c','Website_Recognition__c', 'Donor_Name__c','Donor_Location__c','Contact__c');
    $query = "SELECT " . implode($fields, ',') .  " FROM opportunity WHERE Specific_Project__c = '$id' AND Website_Recognition__c = true AND RecordTypeId = '$recordTypeID' ";
    $results = $this->sf_query($query);
    return $results;
  }

  function get_funding_partners( $id = 0 ){

    global $ph_salesforce;

    if( !$id ){
      $id = $ph_salesforce->current_project_sf_id;
    }

    $recordTypeID = '012F0000000mwNE';
    $fields = array('Funder_Website__c', 'Donor_Name__c', 'Contact__c');
    $query = "SELECT " . implode($fields, ',') .  " FROM opportunity WHERE Specific_Project__c = '$id' AND Website_Recognition__c = true AND RecordTypeId = '$recordTypeID' ";
    $results = $this->sf_query($query);
    return $results;
  }

  function get_project_work_records( $id = 0 ){

    global $ph_salesforce;

    if( !$id ){
      $id = $ph_salesforce->current_project_sf_id;
    }

    $fields = array(
      'Volunteer_Registration__c', // << This ID represents the individual volunteer
      'Site_Manager__c',
      'Site_Manager_2__c',
      'Site_Manager_3__c',
      'Vol_Org_ID__c' // << This ID represents the Vol. Organization ID used in the Accounts table
    );
    $query = "SELECT " . implode($fields, ',') . " FROM Work_Record__c WHERE Project_Information__c = '$id'";
    $results = $this->sf_query($query);
    $return_obj = array(
      'Site_Managers' => array(),
      'Volunteer_Registrations' => array(),
      'Volunteer_Orgs' => array()
    );
    if( count( $results ) ){
      foreach( $results as $result ){
        if( !is_null( $result['Volunteer_Registration__c'] ) && 
            !in_array( $result['Volunteer_Registration__c'], $return_obj['Volunteer_Registrations'] )
          ){
          $return_obj['Volunteer_Registrations'][] = $result['Volunteer_Registration__c'];
        }
        if(!in_array($result['Site_Manager__c'],$return_obj['Site_Managers']) && $result['Site_Manager__c'] != ''){
          $return_obj['Site_Managers'][] = $result['Site_Manager__c'];
        }
        if(!in_array($result['Site_Manager_2__c'],$return_obj['Site_Managers']) && $result['Site_Manager_2__c'] != ''){
          $return_obj['Site_Managers'][] = $result['Site_Manager_2__c'];
        }
        if(!in_array($result['Site_Manager_3__c'],$return_obj['Site_Managers']) && $result['Site_Manager_3__c'] != ''){
          $return_obj['Site_Managers'][] = $result['Site_Manager_3__c'];
        }
        if( !in_array( $result['Vol_Org_ID__c'], $return_obj['Volunteer_Orgs'] ) && !empty( $result['Vol_Org_ID__c'] ) ) {
          $return_obj['Volunteer_Orgs'][] = $result['Vol_Org_ID__c'];
        } 
      }
    }
    return $return_obj;
  }

  function get_all_volunteers(){
    $fields = array('Team_Reg__c', 'id');
    $query = "SELECT " . implode($fields, ',') .  " FROM Volunteer_Registration__c";
    $results = $this->sf_query($query);
  /*   return $results; */
    
    // Convert results so $key == Team_Reg__c
    $vol_hash = array();
    foreach ( $results as $result ) {
      if( isset( $result['Id'] ) )
        $vol_hash[ $result['Id'] ][] = $result;
    }
    return $vol_hash;
  }

  function get_project_volunteers( $id = 0 ){
    $_SESSION['proj_vol']++;
    error_log('proj vol hits : ' . $_SESSION['proj_vol'] . " queries : " . $_SESSION['queries']);
    $fields = array('Team_Reg__c');
    $query = "SELECT " . implode($fields, ',') .  " FROM Volunteer_Registration__c WHERE Id = '$id'";
    $results = $this->sf_query($query);
    return $results;
  }

  function get_project_updates( $id = 0 ) {
    
    global $ph_salesforce;

    if( !$id ){
      $id = $ph_salesforce->current_project_sf_id;
    }

    $fields = array(
      'Date__c',
      'Project_Information__c',
      'Publish_to_website__c',
      'Update_Narrative_website__c',
      'Update_Type__c'
    );
    $query = "SELECT " . implode($fields, ',') .  " FROM Project_Update__c WHERE Project_Information__c = '$id' AND Publish_to_website__c=true ORDER BY Date__c DESC";
    $results = $this->sf_query($query);
    // _log_v(
    //   array('query' => $query, 'results' => $results ) ,
    //   'get_proj_updates() query'
    // );
    return $results;
  }

  function get_contact( $id = 0 ){
    $fields = array('Name', 'Start_Date__c');
    $query = "SELECT " . implode($fields, ',') .  " FROM Contact WHERE Id = '$id'";
    $results = $this->sf_query($query);
    return $results;  
  }

  function get_all_contacts(){
    $fields = array('fst_day__c', 'Account_for_DD__c', 'Account_for_DD_2__c', 'Age__c', 'Alien_or_Admission__c', 'AssistantName', 'AssistantPhone', 'Bank_for_DD__c', 'Bank_for_DD_2__c', 'Birthdate', 'Fax', 'Phone', 'Client_Attributes__c', 'Description', 'Contact_Full_Name__c', 'Contact_ID__c', 'Id', 'Contact_Type__c', 'CreatedById', 'CreatedDate', 'rh2__Currency_Test__c', 'Current_Year_of_Service__c', 'DL_ID_Expiration__c', 'DL_ID_Number__c', 'DL_ID_State__c', 'IsDeleted', 'Department', 'rh2__Describe__c', 'Document_Checklist__c', 'Email', 'EmailBouncedDate', 'EmailBouncedReason', 'Emerg_Contact_1_Email__c', 'Emerg_Contact_1_Phone__c', 'Emerg_Contact_1_Phone_2__c', 'Emerg_Contact_1_Relation__c', 'Emerg_Contact_2_Email__c', 'Emerg_Contact_2_Phone__c', 'Emerg_Contact_2_Phone_Alt__c', 'Emerg_Contact_2_Relation__c', 'Emergency_Contact1__c', 'Emergency_Contact_1_Address__c', 'Emergency_Contact_1_on_trip__c', 'Emergency_Contact_2__c', 'Emergency_Contact_2_Address__c', 'Emergency_Contact_2_on_trip__c', 'Employment_Retention__c', 'End_Date__c', 'Exempt__c', 'FirstName', 'rh2__Formula_Test__c', 'Name', 'Highest_Form_of_Education__c', 'HomePhone', 'I_9_Nationality__c', 'I_9_Reviewer__c', 'rh2__Integer_Test__c', 'Job_Training_Program__c', 'L_4_Block_A__c', 'L_4_Block_B__c', 'L_4_Line_8_Increase_Decrease_of_WH__c', 'LastActivityDate', 'LastModifiedById', 'LastModifiedDate', 'LastName', 'LastCURequestDate', 'LastCUUpdateDate', 'LeadSource', 'List_A_Doc__c', 'List_A_Doc_Title__c', 'List_A_Expiration_Date__c', 'List_A_Issuing_Authority__c', 'List_B_Doc__c', 'List_B_Doc_Title__c', 'List_B_Expiration_Date__c', 'List_B_Issuing_Authority__c', 'List_C_Doc__c', 'List_C_Doc_Title__c', 'List_C_Issuing_Authority__c', 'M_F__c', 'MailingCity', 'MailingCountry', 'MailingState', 'MailingStreet', 'MailingPostalCode', 'Marital_Status__c', 'MasterRecordId', 'Medical_Insurance_Provider__c', 'Medical_Notes__c', 'Middle_Name__c', 'MobilePhone', 'Moved_to_NOLA__c', 'Occupation__c', 'AccountId', 'Origin__c', 'OtherCity', 'OtherCountry', 'OtherPhone', 'OtherState', 'OtherStreet', 'OtherPostalCode', 'Start_Date__c', 'OwnerId', 'PH_Start_Date__c', 'PTO_Recycle_Date__c', 'pd_day_PQ__c', 'Personal_Email__c', 'Phone_Insurance_Provider__c', 'Physician_Name__c', 'Physicians_Phone__c', 'Policy__c', 'Policy_Holder_ID_Num__c', 'Policy_Holder_Name__c', 'Presbytery__c', 'Race__c', 'Recognition_Name__c', 'RecordTypeId', 'Relationship_to_Policy_Holder__c', 'ReportsToId', 'Routing_for_DD__c', 'Routing_for_DD2__c', 'SSN__c', 'Salutation', 'Shirt_Size__c', 'Start_Date_of_Current_Pos__c', 'Status__c', 'Synod__c', 'SystemModstamp', 'Time_Off_Compute__c', 'Title', 'Total_Accrued_Paid_Off__c', 'Total_PTO_Approved_This_Year__c', 'Total_PTO_Days_This_Year__c', 'Total_PTO_Requested_This_Year__c', 'Total_PTO_Used_This_Year__c', 'Total_Vol_Hours__c', 'Traffic_violations_notes__c', 'Traffic_violations_etc_past_5_years__c', 'Deskcom__twitter_username__c', 'Upload_Batch_Name__c', 'Vol_Documents_Collected__c', 'Volunteer_History__c', 'W_4_Line_5_allowances__c', 'W_4_Line_6_additional_withholding__c', 'Website_Bio__c', 'Work_Phone__c', 'Worked_Here_Before__c');
    $query = "SELECT " . implode($fields, ',') .  " FROM Contact ";
    $results = $this->sf_query($query);
    return $results;  
  }

  function get_unsuppressed_employees(){
    $fields = array(
      'Name',
      'Id',
      'Contact_ID__c',
      'Title',
      'Origin__c',
      'Start_Date__c',
      'Work_Phone__c',
      'Email',
      'Website_Bio__c',
      'Suppress_from_Website__c'
    );
    $query = "SELECT " . implode($fields, ',') .  " FROM Contact WHERE Suppress_from_Website__c = false and RecordTypeId = '012F0000000msiv' ";
    $results = $this->sf_query($query);
    return $results;  
  }

  function get_all_accounts(){
    $fields = array('L_T_shirts__c', 'M_T_shirts__c', 'S_T_shirts__c', 'XL_T_shirts__c', 'XS_T_Shirts__c', 'XXL_T_shirts__c', 'XXXL_T_shirts__c', 'AnnualRevenue', 'Primary_Area_of_Interest__c', 'Bal_Due_Invoice_Info_Sent_to_Accounting__c', 'Bal_Due_Invoice_Sent_to_Trip_Leader__c', 'Balance_Due_On_Arrival__c', 'Balance_Due_On_Arrival_Invoice__c', 'Balance_Due_On_Arrival_Notes__c', 'Balance_Received_Notes__c', 'Balance_Received_On_Arrival__c', 'BillingCity', 'BillingCountry', 'BillingState', 'BillingStreet', 'BillingPostalCode', 'Can_Ride_with_Others__c', 'Can_Transport_Others__c', 'City_License_Number__c', 'Community_Tour__c', 'Disaster_Tour_Details__c', 'Contact_Info_Not_coming__c', 'Contact_Info_Trip_Leader__c', 'CreatedById', 'CreatedDate', 'Date_of_Tour__c', 'Days_til_Trip__c', 'IsDeleted', 'Delivery_Area__c', 'Delivery_Fee__c', 'Deposit_Due_Date__c', 'dep_Invoice_Info_Sent_to_Accounting__c', 'Deposit_Invoice_Sent_to_Trip_Leader__c', 'Deposit_Invoice_URL__c', 'Deposit_Notes__c', 'Deposit_Received_Amt__c', 'Deposit_Required_Amt__c', 'Dietary_Restrictions__c', 'Disaster_Tour_del__c', 'NumberOfEmployees', 'End_Date__c', 'End_Date_P1__c', 'Endowment__c', 'Forms_of_Payment__c', 'General_Info_Email__c', 'General_Pres_Email__c', 'General_Sec_Email__c', 'Geographic_Focus_Area__c', 'Giving_Levels__c', 'Giving_Policy__c', 'Grant_Application_Website__c', 'Group_Contact_Not_Coming_to_NOLA__c', 'Group_Contact_for_any_invoices__c', 'Invoice_Contact__c', 'not_on_trip_POC__c', 'Highschool_Group__c', 'Housing_Arrival__c', 'Housing_Departure__c', 'Housing_Invoiced_Message__c', 'Housing_location__c', 'Housing_Payment_Received__c', 'ID_Display__c', 'Industry', 'Informed_of_Deployment_Arrival__c', 'Invoiced_for_Housing__c', 'Invoiced_for_Tour__c', 'LastActivityDate', 'LastModifiedById', 'LastModifiedDate', 'Level_of_Activity__c', 'Login_Site__c', 'Lunch_Details__c', 'MasterRecordId', 'Materials_Available__c', 'Meeting_address__c', 'Meeting_Address_Notes__c', 'Notable_Skills__c', 'Num_of_Tour__c', 'Num_of_Adults__c', 'Num_of_College_Students__c', 'Num_of_Female_Vols__c', 'Num_of_High_Schoolers__c', 'Num_of_Male_Vols__c', 'Num_of_Mid_Schoolers__c', 'Number_of_Chaperones__c', 'Number_of_Underage__c', 'Number_of_Volunteers__c', 'Org_Code__c', 'Description', 'Fax', 'Id', 'Name', 'OwnerId', 'Phone', 'RecordTypeId', 'Type', 'Orientation_Notes__c', 'Orientation_Details__c', 'Orientation_Type__c', 'PNOLA_Housing_Arrival_Instructions__c', 'PNOLA_Housing_Charge__c', 'PNOLA_Housing_Details__c', 'PH_Interactions__c', 'PH_Vol_Charge_Overview__c', 'PNOLA_Active_Account__c', 'PNOLA_Login_Password__c', 'Username__c', 'ParentId', 'IsPartner', 'Partner_Resources__c', 'Password__c', 'President__c', 'Prompted_for_Skill_Sheet__c', 'Regular_meeting_date_time__c', 'Reminded_Bedding__c', 'Reminded_First_Aid__c', 'Reminded_Med_Release__c', 'Ride_With_Message__c', 'Secretary__c', 'Service_Learning_Other_charges__c', 'Service_Learning_Other_Details__c', 'Service_Learning_Other_Invoiced__c', 'Services_Provided__c', 'ShippingCity', 'ShippingCountry', 'ShippingState', 'ShippingStreet', 'ShippingPostalCode', 'Skill_Sheet__c', 'Skills__c', 'Start_Date__c', 'State_License_Number__c', 'Status__c', 'Substance_free__c', 'SystemModstamp', 'Tax_ID_EIN_Number__c', 'Team_Arrival_Notes__c', 'Team_Composition__c', 'Team_Type__c', 'Team_Vol_Hours__c', 'Total_PH_Charges__c', 'Total_Received__c', 'Total_Shortfall_Surplus__c', 'PNOLA_Tour_Charge__c', 'Transport_Others_Message__c', 'Trip_Leader_Coming_with_Group_to_NOLA__c', 'Typical_Grant_Size__c', 'Vol_Group_Total_Vol_Hours__c', 'Volunteer_Org__c', 'WD1_Half__c', 'W10_Half__c', 'WD2_Half__c', 'WD3_Half__c', 'WD4_Half__c', 'WD5_Half__c', 'WD6_Half__c', 'WD7_Half__c', 'WD8_Half__c', 'WD9_Half__c', 'Website', 'What_s_Next__c', 'Will_Give_To__c', 'WD_1__c', 'WD_10__c', 'WD_2__c', 'WD_3__c', 'WD_4__c', 'WD_5__c', 'WD_6__c', 'WD_7__c', 'WD_8__c', 'WD_9__c', 'Transportation__c', 'Work_Days_Notes__c', 'Work_Days_Overview__c', 'Yearly_Giving__c','Team_Reg_Location__c');

    $fields = array(
      'Description',
      'Id',
      'Name',
      'Start_Date__c',
      'Team_Reg_Location__c'
      );    
    $query = "SELECT " . implode($fields, ',') .  " FROM Account ";
    _mem_v( __FUNCTION__ . '() L ' . __LINE__ );
    $results = $this->sf_query($query);
    _mem_v( __FUNCTION__ . '() L ' . __LINE__ );
    return $results;  
  }


  function get_all_volunteer_orgs() {

    // This is Sforce signifier that this account is a volunteer org/group/organization
    // (In the Sforce `Accounts` table the `RecordTypeId` = 012F0000000msf8IAA)
    $vol_org_recordTypeId = '012F0000000msf8IAA';

    $fields = array(
      'Name',
      'Id',
      'Vol_Group_Total_Vol_Hours__c', // total hours worked
      'Vol_Group_Num_Trips__c', // total num of trips
      'Vol_Group_Num_Vols__c', // total num of vols
      'Last_Trip_Vol_Group__c', // last trip date
    );    
    $query = "SELECT " . implode($fields, ',') .
    " FROM Account WHERE RecordTypeId = '{$vol_org_recordTypeId}'".
    " AND IsDeleted != true " .
    " AND Volunteer_Org__c = NULL " . // If the Volunteer_Org__c field has something in it, it's either an Individual Volunteer or a test input
    "";
    $vol_orgs = $this->sf_query( $query );
    return $vol_orgs;  

  } // get_all_volunteer_groups


  function test_projects(){
    global $ph_sforce_connection;
    $sql = "SELECT Flickr_Link__c, Project_SF_ID__c, Name, Marketing_Story__c, constructionstatus__c, Start_Date__c, Completion_Date__c, Volunteer_Hours__c, Program__c, proj_cm__c, Current_Phase__c, Completion__c, Estimated_Market_Cost__c, Estimated_Savings__c, Total_Non_Client_Funds__c, Final_Project_Cost__c, Total_Donations_Needed__c, Total_Donations_Received__c, Scope_of_Work_website__c, Neighborhood__c, Total_Donation_Goal__c, Homeowner_Story_Blog_Post__c, Construction_Cost_Code__c, CreatedById, CreatedDate, IsDeleted, Homeowner__c, Job_Number__c, LastActivityDate, LastModifiedById, LastModifiedDate, Notes__c, Organization__c, OwnerId, Primary_Work_Site_Manager__c, Project_Address__c, Id, SystemModstamp, Total_Donated_Materials__c, Units__c, Work_Site_Manager_2__c, Work_Site_Manager_3__c, Work_Site_Manager_4__c, Work_Site_Manager_5__c, (SELECT Contact__c,Dedication_Honoree_Name__c,Website_Recognition__c,Amount,Check_Date__c FROM Opportunities__r), (SELECT Site_Manager_3__c,Site_Manager_2__c,Volunteer_Registration__c,Site_Manager__c FROM Work_Records__r), (SELECT Team_Reg__c FROM Volunteer_Registrations4__r) FROM Project_Information__c";
    $response = $ph_sforce_connection->query($sql);
    $results = array();
    foreach ($response as $response_key => $record){
      $sObject = new SObject($record);

      $results[$response_key] = get_object_vars($sObject->fields);

      if(isset($sObject->queryResult)){
        foreach($sObject->queryResult as $queryResult_key => $QueryResult){
          foreach($QueryResult as $record){
            $results[$response_key][$record->type] = get_object_vars(new sObject($record));
          }
        }
      }
      $results[] = $sObject;
    }
    return $results;
  }

  function sf_query($sql){
    global $ph_sforce_connection,
           $ph_salesforce;

    $cache_file = md5($sql);
    $cache_base_dir = $ph_salesforce->plugin_path . "query-cache/";

    // $cache_dir = $cache_base_dir . "sf_cache_". $hour_stamp . "/";
    $cache_dir = $this->get_target_cache_dir( $cache_base_dir );


    $cache = $cache_dir . $cache_file;

    if( !is_dir( $cache_base_dir ) ) mkdir( $cache_base_dir );

    if( !is_dir( $cache_dir ) ) mkdir( $cache_dir );  

    if( !file_exists( $cache ) ) {
      $response = $ph_sforce_connection->query($sql); // Save this line when disabling caching
      $_SESSION['queries']++; // Save this line when disabling caching
      file_put_contents($cache, serialize($response));
    } else {
      if(!$response = unserialize(file_get_contents($cache))){
        $response = $ph_sforce_connection->query($sql);
        $_SESSION['queries']++;
      }
    }

    $queryResult = new QueryResult($response);
    $results = array();
    foreach ($queryResult->records as $record) {
      $results[] = $this->convertFields($record->any);
    }  
    return $results;
  }

  function get_target_cache_dir( $cache_base_dir = 0 ) {

    $hour_stamp = date("Y-m-d_H");
    $target_cache_dir = 'TARGET_DEFAULT/';
    
    if ( defined('DO_NOT_QUERY_SF') && DO_NOT_QUERY_SF ) {

      // Run through the general cache folder and use the first folder as target
      $iterator = new DirectoryIterator( $cache_base_dir );
      foreach ( $iterator as $fileinfo ) :
        if ( !$fileinfo->isDot() && $fileinfo->isDir() ) {
          $target_cache_dir = $cache_base_dir . $fileinfo->getFilename() . "/";
        }
      endforeach;

    } else {
      $target_cache_dir =  $cache_base_dir . "sf_cache_". $hour_stamp . "/";
    }

    return $target_cache_dir;

  } // END get_target_cache_dir() 


  function sforce_login($username, $password) {
    global $ph_sforce_connection;
  /*   $wsdl = dirname(__FILE__).'/salesforce-api/soapclient/partner.wsdl.xml'; */
    $wsdl = dirname( __FILE__ ) . '/wsdl.xml';
    $mylogin = null;
    try {
      $ph_sforce_connection = new SforcePartnerClient();
      $ph_sforce_connection->createConnection($wsdl);
      $mylogin = $ph_sforce_connection->login($username, $password);
    } catch (Exception $e) {
      global $errors;
      $errors = $e->faultstring;
      echo $errors;
    }
    return $ph_sforce_connection;
  }

  function convertFields($any) {
    $str = preg_replace('{sf:}', '', $any);
    $array = $this->xml2array('<Object xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">'.$str.'</Object>',0);
    $xml = array();

    if ( !count( $array['Object'] ) ){
      return $xml;
    } else {
      if( ! is_array( $array['Object'] ) )
        return;
        
      foreach ($array['Object'] as $k=>$v) {
        $xml[$k] = $v;
      }
      return $xml;
      
    }
  }

  function xml2array($contents, $get_attributes=1) {
    if(!$contents) return array();

    if(!function_exists('xml_parser_create')) {
        return array('not found');
    }

    $parser = xml_parser_create();
    xml_parser_set_option( $parser, XML_OPTION_CASE_FOLDING, 0 );
    xml_parser_set_option( $parser, XML_OPTION_SKIP_WHITE, 1 );
    xml_parse_into_struct( $parser, $contents, $xml_values );
    xml_parser_free( $parser );

    if(!$xml_values) return;//Hmm...

    //Initializations
    $xml_array = array();
    $parents = array();
    $opened_tags = array();
    $arr = array();

    $current = &$xml_array;

    //Go through the tags.
    foreach($xml_values as $data) {
      unset($attributes,$value);//Remove existing values, or there will be trouble

      //This command will extract these variables into the foreach scope
      // tag(string), type(string), level(int), attributes(array).
      extract($data);//We could use the array by itself, but this cooler.

      $result = '';
      if($get_attributes) {//The second argument of the function decides this.
        $result = array();
        if(isset($value)) $result['value'] = $value;

        //Set the attributes too.
        if(isset($attributes)) {
          foreach($attributes as $attr => $val) {
            if($get_attributes == 1) $result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr'
            /**  :TODO: should we change the key name to '_attr'? Someone may use the tagname 'attr'. Same goes for 'value' too */
          }
        }
      } elseif(isset($value)) {
          $result = $value;
      }

      //See tag status and do the needed.
      if($type == "open") {//The starting of the tag '<tag>'
          $parent[$level-1] = &$current;

          if(!is_array($current) or (!in_array($tag, array_keys($current)))) { //Insert New tag
              $current[$tag] = $result;
              $current = &$current[$tag];

          } else { //There was another element with the same tag name
              if(isset($current[$tag][0])) {
                  array_push($current[$tag], $result);
              } else {
                  $current[$tag] = array($current[$tag],$result);
              }
              $last = count($current[$tag]) - 1;
              $current = &$current[$tag][$last];
          }

      } elseif($type == "complete") { //Tags that ends in 1 line '<tag />'
          //See if the key is already taken.
          if(!isset($current[$tag])) { //New Key
              $current[$tag] = $result;

          } else { //If taken, put all things inside a list(array)
              if((is_array($current[$tag]) and $get_attributes == 0)//If it is already an array...
                      or (isset($current[$tag][0]) and is_array($current[$tag][0]) and $get_attributes == 1)) {
                  array_push($current[$tag],$result); // ...push the new element into that array.
              } else { //If it is not an array...
                  $current[$tag] = array($current[$tag],$result); //...Make it an array using using the existing value and the new value
              }
          }

      } elseif($type == 'close') { //End of tag '</tag>'
          $current = &$parent[$level-1];
      }
    }

    return($xml_array);
  } 

  function get_project_ids( $projects ) {
    $projectIds = array();
    foreach( $projects as $key => $project ) {
      $projectIds[] = "'{$project['Project_SF_ID__c']}'";
    }
    return $projectIds;
    
  }
} // end PH_Salesforce_API class def
} // end if !class_exists...

// if( class_exists('PH_Salesforce_API') ) {

//   global $ph_sf_api;
  
//   $ph_sf_api = new PH_Salesforce_API();

// } // end if( class_exists('PH_Salesforce_API') )
