<?php

/**
 * PH_Project Class Definition
 * Provides getter/setter functions for project data
 */

if ( ! class_exists('PH_Project')) {

class PH_Project {

  const CPT_NAME      = 'ph_sf_projects';

  const ID_KEY        = 'ph_sf_project_id';
  const DETAILS_KEY   = 'ph_sf_project_details';
  const TIMESTAMP_KEY = 'ph_sf_project_timestamp';

  public $wp_id       = null;

  public $sf_id;

  public $meta = '';
  public $details = '';

  function __construct( $args = null ) {

    global $post;

    if ( isset( $args['wp_id'] ) ) :
      $this->wp_id = $args['wp_id'];
    elseif ( !empty( $post ) ) :
      $this->wp_id = $post->ID;
    endif;

    $this->details = isset( $args['details'] ) ? $args['details'] : $this->the_details();

    $this->sf_id = isset( $args['details']->Project_SF_ID__c ) ? $args['details']->Project_SF_ID__c : $this->get_sf_id() ;

  } // END __construct()


  function detail( $detail_name = 0 ) {

    if( !$detail_name ) return false;

    $details = (object) $this->the_details();

    return isset( $details->{$detail_name} ) ? $details->{$detail_name} : false;

  } // END detail()


  function the_details() {

    global $post;

    // Check to see if there's global post data
    if ( empty( $post ) ) :

      // If not, use the object's details parsed from SForce
      $meta = array(
        self::ID_KEY => $this->details->Project_SF_ID__c,
        self::DETAILS_KEY => (array) $this->details
      );

    else : 
      // Otherwise, call the db to ge the meta
      $meta = $this->the_meta();
    endif;

    // The details are either pulled from the DB ( $meta[ self::DETAILS_KEY ][0] )
    // OR from the employee obj itself $meta[ self::DETAILS_KEY ]
    $details = isset( $meta[ self::DETAILS_KEY ][0] ) ? $meta[ self::DETAILS_KEY ][0] : $meta[ self::DETAILS_KEY ];

    return $details ? $details : new stdClass();

  } // END the_details() 

  function the_meta( $action = 'get' ) {

    global $ph_salesforce;

    // Try using instance var first
    $meta = $this->meta;

    if( empty( $meta ) ) {

      // If it's empty, try PH_Salesforce saved meta
      $saved_meta = isset( $ph_salesforce->project_meta[ $this->wp_id ] ) ? $ph_salesforce->project_meta[ $this->wp_id ] : false ;

      // Return saved_meta if it exists or call the DB to get it
      $meta = !empty( $saved_meta ) ? $saved_meta : $this->project_meta( $this->wp_id, $action );
    }

    return $meta;

  } // END the_meta() 


  function project_meta( $post_ID = 0, $action = 'get', $proj_obj = NULL ) {

    global $ph_salesforce;

    if( !$post_ID ){
      global $post;
      if( isset( $post->ID ) ) {
        $post_ID = $post->ID;
      }
    }
    
    date_default_timezone_set('America/Chicago');
    
    if( !$proj_obj ) :
      
      if( isset( $ph_salesforce->the_project ) ) :
        $proj_obj = $ph_salesforce->the_project;
      else :
        $ph_salesforce->the_project = $this;
        $proj_obj = $ph_salesforce->the_project;      
      endif;

    endif;

    // Don't mess with the details key if we're trying to get the meta
    if( $action !== 'get' ) {
      $proj_details = $proj_obj->details;

      $proj_details->wp_id = $post_ID;
    }

    switch ( $action ) :

      case 'insert':
        if( !$proj_details ) return false; // If nothing is given to insert, end here
          
        // _log('INSERTING project meta ' . $proj_details->Name ); 
      
        add_post_meta( $post_ID, self::ID_KEY, $this->get_sf_id(), true );
        add_post_meta( $post_ID, self::DETAILS_KEY, $this->details, true );
        break;

      case 'update' :
        if ( !$proj_details ) return false; //If nothing is given to update, end here

        // _log('UPDATING project meta ' . $proj_details->Name );         
        update_post_meta( $post_ID, self::TIMESTAMP_KEY, date("Y-m-d g:i:s A") );
        update_post_meta( $post_ID, self::ID_KEY, $this->get_sf_id() );
        update_post_meta( $post_ID, self::DETAILS_KEY, $this->details );
        break;

      case 'delete' :
        delete_post_meta( $post_ID, self::DETAILS_KEY );
        delete_post_meta( $post_ID, self::ID_KEY );
        delete_post_meta( $post_ID, self::TIMESTAMP_KEY );
        break;

      case 'get' :
        $project_meta[ self::ID_KEY ] = get_post_meta( $post_ID, self::ID_KEY );
        $project_meta[ self::DETAILS_KEY ] = get_post_meta( $post_ID, self::DETAILS_KEY );
        
        return $project_meta;
      default :
        return false;
        break;

    endswitch;

    return true;

  } //end function project_meta


  function get_wp_id() {

    $sf_id = $this->get_sf_id();

    if( !$sf_id ) {
      return false;
    }

    // get the post id
    $args = array(
      "meta_key" => self::ID_KEY,
      "meta_value" => $sf_id,
      "post_status" => array('pending', 'publish', 'future')
    );
    $project_query = cpt_query( self::CPT_NAME, 1, $args );       

    $this->wp_id = $project_query->found_posts > 0 ? $project_query->posts[0]->ID : false ;

    return $this->wp_id;

  } // END get_wp_id()


  static function get_project_from_sf_id( $sf_id = 0 ) {

    if( !$sf_id ) return false;

    // get the post id
    $args = array(
      "meta_key" => self::ID_KEY,
      "meta_value" => $sf_id,
      "post_status" => array('pending','publish', 'future')
    );
    $project_query = cpt_query( self::CPT_NAME, 1, $args );       

    return $project_query->found_posts > 0 ? $project_query->posts[0] : false ;

  } // END get_project_from_sf_id() 



  function get_sf_id() {

   return isset( $this->sf_id ) ? $this->sf_id : $this->get_sf_id_from_db();

  } // END get_sf_id() 


  // Meta Query to get SF id
  function get_sf_id_from_db() {

    if( !$this->wp_id ) return false;

    $meta = $this->the_meta();

    return isset( $meta[ self::ID_KEY ] ) ? $meta[ self::ID_KEY ] : false;

  } // END get_sf_id_from_db() 



  function create_in_wp() {
    
    if ( $this->get_wp_id() ) return false; // Return if already exists in WP

    $employee_name = $this->detail('Name');
    
    // create a new Employee Post
    $postArgs = array(
      "post_type" => self::CPT_NAME,
      "post_title" => $employee_name,
      "post_status" => "pending",
      "post_content" => $this->detail('Marketing_Story__c'),
    );

    $post_ID = wp_insert_post( $postArgs );
    
    _log("Creating new project {$employee_name}" );
    
    $this->wp_id = $post_ID;

    // _log( $postArgs, "new_wp_employee() Post ID: {$post_ID}" );
    
    $this->project_meta( $post_ID, 'insert' );

  } // END create_in_wp() 


  function update_in_wp() {

    $post = array(
      'ID' => $this->wp_id,
      "post_title" => $this->detail('Name'),
      "post_content" => $this->detail('Marketing_Story__c'),
    );

    wp_update_post( $post );

    $this->the_meta('update');
    
  } // END update_in_wp() 



  function get_site_managers() {
    $work_records = $this->detail('work_records');

    return isset( $work_records->Site_Managers ) ? $work_records->Site_Managers : false ;

  } // END get_site_managers() 


  function percentage_bar_class($value='') {

    $percent_complete = floatval( $this->detail('Completion__c') );

    $bar_class = "progress-bar-warning";    

    if ( $percent_complete >= 80)
      $bar_class = "";

    if ( $percent_complete == 100 )
      $bar_class = "progress-bar-success";

    return $bar_class;

  } // END get_percentage_bar_class() 

} // end class PH_Project

} // END class_exists

// if( class_exists('PH_Project') ) {

//   global $ph_project;
  
//   $ph_project = new PH_Project();

// } // end if( class_exists('PH_Project') )

?>