<?php

/**
 * PH_Volunteer_Org Class Definition
 * Provides getter/setter functions for Volunteer Org data
 */

if ( ! class_exists('PH_Volunteer_Org')) {

class PH_Volunteer_Org {

  private $wp_id;

  private $sf_id;

  private $meta = '';
  private $details = '';

  const CPT_NAME      = 'ph_sf_vol_orgs';

  const ID_KEY        = 'ph_sf_vol_org_id';
  const DETAILS_KEY   = 'ph_sf_vol_org_details';
  const TIMESTAMP_KEY = 'ph_sf_vol_org_timestamp';


  function __construct( $args = null ) {

    global $post;

    $this->wp_id = isset( $args['wp_id'] ) ? $args['wp_id'] :
      empty( $post ) ? null : $post->ID ;

    $this->details = isset( $args['details'] ) ? $args['details'] : $this->the_meta();

    $this->sf_id = isset( $args['details']->Id ) ? $args['details']->Id : null ;

  } // END __construct()


  function the_meta() {

    global $ph_salesforce;

    // Try using instance var first
    $meta = $this->meta;

    if( empty( $meta ) ) {

      // If it's empty, try PH_Salesforce saved meta
      $saved_meta = isset( $ph_salesforce->vol_orgs_meta[ $this->wp_id ] ) ? $ph_salesforce->vol_orgs_meta[ $this->wp_id ] : false ;

      // Return saved_meta if it exists or call the DB to get it
      $meta = !empty( $saved_meta ) ? $saved_meta : $this->vol_org_meta( $this->wp_id );
    }

    return $meta;

  } // END the_meta() 


  function the_details() {

    global $post;

    // Check to see if there's global post data
    // Either its an empty global post
    if ( empty( $post ) ) :

      // If not, use the object's details parsed from SForce
      $meta = array(
        self::ID_KEY => $this->details->Id,
        self::DETAILS_KEY => (array) $this->details
      );

    // Or the global post is not an vol_org, but we already have details
    elseif ( !empty( $this->details ) && self::CPT_NAME !== $post->post_type ) :
    
      $meta = $this->details;

    else : 
      // Otherwise, call the db to ge the meta
      $meta = $this->the_meta();
    endif;

    // The details are either pulled from the DB ( $meta[ self::DETAILS_KEY ][0] )
    // OR from the vol_org obj itself $meta[ self::DETAILS_KEY ]
    $details = isset( $meta[ self::DETAILS_KEY ][0] ) ? $meta[ self::DETAILS_KEY ][0] : $meta[ self::DETAILS_KEY ];

    return $details ? $details : new stdClass();

  } // END the_details() 


  function detail( $detail_name = 0 ) {

    if( !$detail_name ) return false;

    $details = (object) $this->the_details();

    return isset( $details->{$detail_name} ) ? $details->{$detail_name} : false;

  } // END detail() 


  function print_detail( $detail_name, $before = '', $after = '' ) {

    $detail = $this->detail( $detail_name );

    if( !$detail ) return false;

    $inner = ( $detail_name == "Start_Date__c" ) ? date('Y', strtotime( $detail ) ) : $detail;
    ?>
    
    <span class="<?php echo strtolower( str_replace( "__c", "", $detail_name ) ); ?>"><?php echo $before . $inner . $after; ?></span>
    <?php 

  } // END print_detail()

  function first_name() {
    global $post;

    if( !$post ) return false;

    $first_name = strtok( get_the_title(),  ' ' );

    return $first_name;
  } // END first_name() 


  function create_in_wp() {
    
    if ( $this->get_wp_id() ) return false; // Return if already exists in WP
    
    // create a new Volunteer Org Post
    $postArgs = array(
      "post_type" => self::CPT_NAME,
      "post_title" => $this->detail('Name'),
      "post_status" => "publish"
    );

    $post_ID = wp_insert_post( $postArgs );
    
    $this->wp_id = $post_ID;

    // $this->set_categories();

    // _log( $postArgs, "new_wp_vol_org() Post ID: {$post_ID}" );
    
    $this->vol_org_meta( $post_ID, 'insert' );

  } // END create_in_wp() 


  function update_in_wp() {

    $post = array(
      'ID' => $this->wp_id,
      "post_title" => $this->detail('Name')
    );

    wp_update_post( $post );

    // $this->set_categories();

    $this->vol_org_meta( $this->wp_id, 'update' );
    
  } // END update_in_wp() 


  function get_wp_id() {

    $sf_id = $this->get_sf_id();

    if( !$sf_id ) {
      return false;
    }

    $vol_org = self::get_vol_org_from_sf_id( $sf_id );

    return $this->wp_id = $vol_org ? $vol_org->ID : false;

  } // END get_wp_id()


  function set_categories() {

    if( !$this->wp_id) return false;

    $vol_org_cats = array();

    $vol_org_cats = wp_get_post_categories( $this->wp_id );

    $name_parts = explode( " ", $this->detail('Name') ); 
    $f_name = $name_parts[0];

    // Get the ID of the "Staff Members" category
    $staff_members_cat_obj = get_category_by_slug('staff-members'); 
    
    // Get all child categories under "Staff Members" parent cat
    $args = array(
      'child_of' => $staff_members_cat_obj->term_id,
    );
    $all_vol_orgs_cats_objs = get_categories( $args );

    // Iterate through all Staff Member categories.
    // If one matches the first name, assign it
    foreach ( $all_vol_orgs_cats_objs as $cat_obj ) {
      if( sanitize_title( $f_name ) == $cat_obj->slug  )
        $vol_org_cats[] = $cat_obj->term_id;
    }

    $append=true; // Append categories to existing array
    return wp_set_post_terms( $this->wp_id, $vol_org_cats, 'category', $append );

  } // END set_categories() 


  static function get_vol_org_from_sf_id( $sf_id = 0 ) {

    if( !$sf_id ) return false;

    // get the post id
    $args = array(
      "meta_key" => self::ID_KEY,
      "meta_value" => $sf_id,
      "post_status" => array('pending', 'publish', 'future')
    );
    $vol_orgs_query = cpt_query( self::CPT_NAME, 1, $args );       

    return $vol_orgs_query->found_posts > 0 ? $vol_orgs_query->posts[0] : false ;

  } // END get_vol_org_from_sf_id() 


  function get_sf_id() {

   return isset( $this->sf_id ) ? $this->sf_id : $this->get_sf_id_from_db();

  } // END get_sf_id() 


  // Meta Query to get SF id
  function get_sf_id_from_db() {

    if( !$this->wp_id ) return false;

    $meta = $this->the_meta();

    return isset( $meta[ self::ID_KEY ] ) ? $meta[ self::ID_KEY ] : false;

  } // END get_sf_id_from_db() 


  // Get recent Project Posts for this vol_orgs
  function get_recent_projects( $posts_per_page = 3 ) {

    $related_projects = $this->detail('related_projects');

    if( !$related_projects ) return false;

    $args = array(
      'post__in' => $related_projects
    );
    $projects_query = cpt_query( PH_Project::CPT_NAME, $posts_per_page , $args );

    return $projects_query;
    
  } // END get_recent_projects() 


  function vol_org_meta( $post_ID, $action = 'get', $vol_org_obj=NULL) {

    global $ph_salesforce;
  
    if ( !$post_ID ) {
      global $post;
      $post_ID = $post->ID;
    }
    
    if( !$vol_org_obj ) :
      
      if( isset( $ph_salesforce->the_vol_org ) ) :
        $vol_org_obj = $ph_salesforce->the_vol_org;
      else :
        $ph_salesforce->the_vol_org = $this;
        $vol_org_obj = $ph_salesforce->the_vol_org;      
      endif;

    endif;

    // Don't mess with the details key if we're trying to get the meta
    if( $action !== 'get' ) {
      $vol_org_details = $vol_org_obj->details;

      $vol_org_details->wp_id = $post_ID;
    }
                
    //Let's make a switch to handle the three cases of 'Action'
    switch ($action) :

      case 'insert':
        if( !$vol_org_details ) return false; // If nothing is given to update, end here
          
        // _log('inserting vol_org ' . $vol_org_details->Name ); 
        _mem('inserting vol_org ' . $vol_org_details->Name ); 
      
        add_post_meta( $post_ID, self::ID_KEY, $this->get_sf_id(), true );
        add_post_meta( $post_ID, self::DETAILS_KEY, $this->details, true );
        break;

      case 'update' :
        if( !$vol_org_details ) return false; //If nothing is given to update, end here
      
        update_post_meta( $post_ID, self::ID_KEY, $this->get_sf_id() );
        update_post_meta( $post_ID, self::DETAILS_KEY, $vol_org_details );        
        break;

      case 'delete' :
        delete_post_meta( $post_ID, self::ID_KEY );
        delete_post_meta( $post_ID, self::DETAILS_KEY );
        break;

      case 'get' :
        $vol_org_meta[ self::ID_KEY ] = get_post_meta( $post_ID, self::ID_KEY );
        $vol_org_meta[ self::DETAILS_KEY ] = get_post_meta( $post_ID, self::DETAILS_KEY  );

        // Save this for later reference
        $ph_salesforce->vol_org_meta[ $post_ID ] = $vol_org_meta;

        return $vol_org_meta;

      default :
        return false;
        break;

    endswitch;

    return true;
    
  } // END vol_org_meta()


} // end class PH_Volunteer_Org

} // END class_exists

// if( class_exists('PH_Volunteer_Org') ) {

//   global $PH_Volunteer_Org;
  
//   $PH_Volunteer_Org = new PH_Volunteer_Org();

// } // end if( class_exists('PH_Volunteer_Org') )

?>