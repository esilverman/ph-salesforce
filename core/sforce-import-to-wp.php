<?php

date_default_timezone_set('America/Chicago');

ini_set('memory_limit','512M'); // HostGator shared maxes out at 256M


//save_sforce_data(FALSE); // Initiate on page load #### For testing only 

global $query_count;
$query_count = 0;

add_action('wp', 'setup_scheduled_event');

function setup_scheduled_event() {
  if ( !wp_next_scheduled( 'save_data_to_db' ) ) {
    wp_schedule_event( time(), 'hourly', 'save_data_to_db');
  }
}

//Schedule JSON save to DB hourly from SalesForce
add_action('save_data_to_db', 'save_sforce_data');

 // wp_clear_scheduled_hook( 'save_data_to_db' );  


// Bring Salesforce data into Wordpress table and relate it to CPT objects
function save_sforce_data() { 

  // do something every hour
  unset($log);
  $log =  ( defined( 'DOING_CRON' ) && DOING_CRON )
            ? "save_sforce_data() Running From cron "
            : "save_sforce_data() : Run from command line...";
            
  _log( $log );
  
  // This will query Salesforce's API and update the projects.json & staff.json files
  require_once dirname( __FILE__ ) . '/save-as-json.php'; // SalesForce-Interface
  
  // Get projects.json as string
  $projectsJSON = file_get_contents( plugin_dir_path( __FILE__ ) . "/json/projects.json" );
   // console_log(json_decode($projectsJSON), 'sforceProjects');   

  // Get projects.json as string
  $staffJSON = file_get_contents( plugin_dir_path( __FILE__ ) . "/json/staff.json" );

  // Convert Projects, Staff & Staff Projects to PHP array
  $sforceProjects = json_decode($projectsJSON);
  $sforceStaff = json_decode($staffJSON);

  // Grab timestamp and remove from the array
  $projectsTimestamp = $sforceProjects->timestamp;
  $staffTimestamp = $sforceStaff->timestamp;
  unset($sforceProjects->timestamp, $sforceStaff->timestamp);

  // Get list of projects saved to WP
  $sf_projects_saved_to_wp = get_option( 'sf_projects_saved_to_wp' );
  
  // Get list of staff saved to WP
  $sf_staff_saved_to_wp = get_option( 'sf_staff_saved_to_wp' );

  $staffProjects = array();
  
  // Iterate through the sforceProjects and 
  // check if each one has an associated Project post
  if ( $sforceProjects ):
    foreach ( $sforceProjects as $project ) :
      // _mem( __FUNCTION__ . '() L ' . __LINE__ );
    
      $sforceProjectID = $project->Project_SF_ID__c;

      add_project_to_staff_records( $project, $staffProjects );
      
      add_or_update_project_meta( $project, $sf_projects_saved_to_wp );
      
    endforeach;
  endif;

  // Iterate through the sforceStaff and 
  // check if each one has an associated Staff post  
  if ($sforceStaff):
    foreach ($sforceStaff as $staff) :
      // _mem( __FUNCTION__ . '() L ' . __LINE__ );
  
      if( empty($staff->Id) )
          return false;
      
      $sforceStaffID = $staff->Id;
  
      // If the Staff Projects obj has records for this Staff Key
      if ( !empty( $staffProjects[ $sforceStaffID ] ) ) {
        // add the project IDs to the Staff Obj
        $staff->_wp_related_projects = $staffProjects[ $sforceStaffID ]->_wp_related_projects;
      }
  
      add_or_update_staff_meta( $staff, $sf_staff_saved_to_wp );
  
    endforeach;
  endif;

  // Update the list of projects using the newly-updated list
  update_option('sf_projects_saved_to_wp', $sf_projects_saved_to_wp);
  update_option('sf_last_projects_update', $projectsTimestamp);
  
  // Update the list of staff using the newly-updated list
  update_option('sf_staff_saved_to_wp', $sf_staff_saved_to_wp);
  update_option('sf_last_staff_update', $staffTimestamp);
  
  
   // console_log(get_option('cron'), 'cron'); 


} // end save sforce_data


function add_or_update_project_meta( $project, &$sf_projects_saved_to_wp ) {

    // Grab project ID from Sforce    
    $sforceProjectID = $project->Project_SF_ID__c;    

    // if there's alreay a Project Post with this ID in it's post_meta
    if ( $sf_projects_saved_to_wp && in_array( $sforceProjectID, $sf_projects_saved_to_wp ) ) :
    
      // get the post id
      $args = array(
        "post_type" => "projects",
        "meta_key" => "_sforce_project_id",
        "meta_value" => $sforceProjectID,
      );
      $projectQuery = new WP_Query( $args );

      // Prevent looking through a non-existent record
      if( empty( $projectQuery->posts ) ) {
        return error_log('nothing found in this post : ' . __FUNCTION__ . '() L' . __LINE__ . ' ' . __FILE__ );
      }

      $post_id = $projectQuery->posts[0]->ID;       
      
      // replace the existing WordPress Project Details with
      // the newly grabbed salesforce Data
      update_post_meta( $post_id, '_sforce_project_details', $project );
      update_post_meta( $post_id, '_sforce_project_timestamp', date("Y-m-d g:i:s A") );
            
      
    else :
       // console_log($sforceProjectID,'adding new post'); 
      
      // create a new Project Post
      $postArgs = array(
        "post_type" => "projects",
        "post_title" => $project->Name,
        "post_status" => "publish"
      );
      $post_id = wp_insert_post( $postArgs );

      // Add the sforceProjectID & projectObj as post_meta
      project_meta( $post_id, 'update', $sforceProjectID, $project);
      
      // Add the salesforce project ID to list of saved projects
      $sf_projects_saved_to_wp[] = $sforceProjectID;
      
    endif;
  
} // end function add_or_update_project_meta()

function project_meta( $post_id, $action = 'get', $sforceProjectID=NULL, $sforce_details=NULL) {
  
  date_default_timezone_set('America/Chicago');
  
  //Let's make a switch to handle the three cases of 'Action'
  switch ($action) {
    case 'update' :
      if( !$sforce_details && !$sforceProjectID )
        //If nothing is given to update, end here
        return false;
      
      //add_post_meta usage:
      //add_post_meta( $post_id, $meta_key, $meta_value, $unique = false )
      
      if( $sforce_details && $sforceProjectID ) {
        add_post_meta( $post_id, '_sforce_project_id', $sforceProjectID, true );
        add_post_meta( $post_id, '_sforce_project_details', $sforce_details, true );
        add_post_meta( $post_id, '_sforce_project_timestamp', date("Y-m-d g:i:s A"), true );
        return true;
      }
    case 'delete' :
      delete_post_meta( $post_id, '_sforce_project_details' );
      delete_post_meta( $post_id, '_sforce_project_id' );
      delete_post_meta( $post_id, '_sforce_project_timestamp' );
    break;
    case 'get' :
      $projectMeta['_sforce_project_id'] = get_post_meta( $post_id, '_sforce_project_id' );
      $projectMeta['_sforce_project_details'] = get_post_meta( $post_id, '_sforce_project_details' );
      
      return $projectMeta;
    default :
      return false;
    break;
  } //end switch
} //end function project_meta

function add_or_update_staff_meta($staff, &$sf_staff_saved_to_wp) {

    // Grab staff ID from Sforce    
    if( empty($staff->Id) )
        return false;

    $sforceStaffID = $staff->Id;    

    // if there's alreay a Staff Post with this ID in it's post_meta
    if ( $sf_staff_saved_to_wp && in_array($sforceStaffID, $sf_staff_saved_to_wp) ) :
      
       // console_log($sforceStaffID,$staff->Name.': already exists'); 
      
      // get the post id
      $args = array(
        "post_type" => "sf_staff",
        "meta_key" => "_sforce_staff_id",
        "meta_value" => $sforceStaffID,
      );
      $staffQuery = new WP_Query( $args );       
      $post_id = $staffQuery->posts[0]->ID;       
      
      // replace the existing WordPress Staff Details with
      // the newly grabbed salesforce Data
      update_post_meta( $post_id, '_sforce_staff_details', $staff );
      update_post_meta( $post_id, '_sforce_staff_timestamp', date("Y-m-d g:i:s A") );
            
      
    else :
       // console_log($sforceStaffID,'adding new post'); 
      
      // create a new Staff Post
      $postArgs = array(
        "post_type" => "sf_staff",
        "post_title" => $staff->Name,
        "post_status" => "publish"
      );
      $post_id = wp_insert_post( $postArgs );

      // Add the sforceStaffID & staffObj as post_meta
      staff_meta( $post_id, 'update', $sforceStaffID, $staff);
      
      // Add the salesforce staff ID to list of saved staff
      $sf_staff_saved_to_wp[] = $sforceStaffID;
      
    endif;
  
} // end function add_or_update_staff_meta()

function staff_meta( $post_id, $action = 'get', $sforceStaffID=NULL, $sforce_details=NULL) {
  
  date_default_timezone_set('America/Chicago');
  
  //Let's make a switch to handle the three cases of 'Action'
  switch ($action) {
    case 'update' :
      if( !$sforce_details && !$sforceStaffID )
        //If nothing is given to update, end here
        return false;
      
      //add_post_meta usage:
      //add_post_meta( $post_id, $meta_key, $meta_value, $unique = false )
      
      if( $sforce_details && $sforceStaffID ) {
        add_post_meta( $post_id, '_sforce_staff_id', $sforceStaffID, true );
        add_post_meta( $post_id, '_sforce_staff_details', $sforce_details, true );
        add_post_meta( $post_id, '_sforce_staff_timestamp', date("Y-m-d g:i:s A"), true );
        return true;
      }
    case 'delete' :
      delete_post_meta( $post_id, '_sforce_staff_details' );
      delete_post_meta( $post_id, '_sforce_staff_id' );
      delete_post_meta( $post_id, '_sforce_staff_timestamp' );
    break;
    case 'get' :
      $staffMeta['_sforce_staff_id'] = get_post_meta( $post_id, '_sforce_staff_id' );
      $staffMeta['_sforce_staff_details'] = get_post_meta( $post_id, '_sforce_staff_details' );
      
      return $staffMeta;
    default :
      return false;
    break;
  } //end switch
} //end function staff_meta

function add_project_to_staff_records( $project, &$staffProjects ) {
  global $query_count;
  foreach ( $project->workrecords->Site_Managers as $siteManagerId => $details ) :
    $siteManagerIdPrefix = "003F000";
    if ( !strstr( $siteManagerId, $siteManagerIdPrefix ) ) {
         // console_json($project, 'staff with non string work record'); 
    }
    if ( $siteManagerId !== "_empty_") {

      // get the Project id
      $project_id = "";
      $args = array(
        "post_type" => "projects",
        "meta_key" => "_sforce_project_id",
        "meta_value" => $project->Project_SF_ID__c,
      );
      $projectQuery = new WP_Query( $args );

      if ( $projectQuery->posts ) {
        $project_id = $projectQuery->posts[0]->ID;       
          
        $staffProjects[ $siteManagerId ]->_wp_related_projects[] = $project_id;
      }

    }
  endforeach;

}//end function add_project_to_staff_records

?>