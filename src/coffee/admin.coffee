
jQuery.fn.exists = ->
  @length > 0

$ = jQuery
$ ->


  $('.manual-refresh-btn').on 'click', (e) ->
    e.preventDefault()
    data = action: 'ph_sf_refresh_all'
    $('.spinner').toggleClass 'hidden'
    $('.response').hide()
    $('.response-list').empty()
    $.ajax 
      url : ajaxurl
      data : data
      type : 'POST'
    .done (r) ->
      $('.spinner').toggleClass 'hidden'
      import_results = $.parseJSON(r)
      projects = import_results.projects
      employees = import_results.employees
      response_list = $('ul.response-list')
      updated_project_names = []
      updated_employee_names = []

      if typeof projects.created isnt "undefined"
        h3 = $('<h3/>').text('Newly Imported Projects').appendTo( response_list )
        $.each projects.created, (i) ->
          details = projects.created[i].details
          li = $('<li/>').addClass('project created').text('[ ' + details.Name + ' ] ').appendTo( response_list )
          return

      if typeof projects.updated isnt "undefined"
        h3 = $('<h3/>').text('Updated Projects').appendTo( response_list )
        $.each projects.updated, (i) ->
          details = projects.updated[i].details
          updated_project_names.push details.Name
          return
        $('<p/>').addClass('updated-projects').text( updated_project_names.join ', ' ).appendTo( response_list )

      if typeof employees.created isnt "undefined"
        h3 = $('<h3/>').text('Newly Imported Employees').appendTo( response_list )
        $.each employees.created, (i) ->
          li = $('<li/>').addClass('employee created').text('[ ' + employees.created[i] + ' ] ').appendTo( response_list )
          return

      if typeof employees.updated isnt "undefined"
        h3 = $('<h3/>').text('Updated Employees').appendTo( response_list )
        $.each employees.updated, (i) ->
          updated_employee_names.push employees.updated[i]
          return
        $('<p/>').addClass('updated-employees').text( updated_employee_names.join ', ' ).appendTo( response_list )


      $('.response').show()
      return
    .fail (r) ->
      $('.spinner').toggleClass 'hidden'
      $('.response-list').html('Server response contained an error: "' + r.statusText + '"')
      $('.response').show()
      return

    return # end .manual-refresh-btn 'click'

  $('.show-last-results-btn').on 'click', (e) ->
    $(this).text( (i, text) ->
        text = if text is "Show Results" then "Hide Results" else "Show Results";
      )
    $('.last-results').slideToggle('fast')
    return # end .manual-refresh-btn 'click'

  return # end $ ->
